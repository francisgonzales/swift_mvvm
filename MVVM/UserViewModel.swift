//
//  UserViewModel.swift
//  MVVM
//
//  Created by Francis Gonzales on 11/9/18.
//  Copyright © 2018 . All rights reserved.
//

class DogViewModel {
    private var user: User
    
    init(firstname: String,lastname: String,emailaddress:String  ) {
        self.user = User(firstname: firstname, lastname:lastname , emailaddress: emailaddress)
    }
    
    var firstName: String {
        return user.FirstName
    }
    
    var lastName: String {
        return user.LastName
    }
    
    
    var emailAddress: String {
        return  user.EmailAddress
    }
    
    
}

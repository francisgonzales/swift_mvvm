//
//  User.swift
//  MVVM
//
//  Created by Francis Gonzales on 11/9/18.
//  Copyright © 2018 . All rights reserved.
//

// Model
class User {
    var FirstName: String
    var LastName: String
    var EmailAddress: String
    
    init(firstname: String, lastname: String, emailaddress: String) {
        FirstName = firstname
        LastName =  lastname
        EmailAddress = emailaddress
        
    }
}
